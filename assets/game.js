
//instâncias do dom
const player = document.querySelector('#player1');    
const btnLeft = document.querySelector('#btn_left');
const btnUp = document.querySelector('#btn_up');
const btnRight = document.querySelector('#btn_right');
const btnDown = document.querySelector('#btn_down');
const btnReset = document.querySelector('#btn_reset');
    
//variáveis auxiliares como coordenadas, velocidade e tamanho do player
let _xx = 0;
let _yy = 0;
let xx = 0;
let yy = 0;
let spd = 16;
let playerSize = 32;
    
//vars delimitadoras do mapa
let mapWidth = document.querySelector('.map').offsetWidth;
let mapHeight = document.querySelector('.map').offsetHeight;

//vars delimitadoras do movimento do player pra ele não sair da tela
const minX = -mapWidth/2;
const minY = -mapHeight/2;
const maxX = mapWidth/2-playerSize;
const maxY = mapHeight/2-playerSize;
    
//instância do localstorage
myStorage = window.localStorage;  

//checa se tem suporte ao localstorage
if(typeof(Storage) !== "undefined") {
       
    //carrega as cordenadas do localstorage ao iniciar
    _xx = parseInt(myStorage.getItem('playerX'));
    _yy = parseInt(myStorage.getItem('playerY'));
    
    if (myStorage.playerX === undefined) _xx=0; 
    if (myStorage.playerX === undefined) _yy=0;

    xx = _xx;
    yy = _yy;

    movePlayer(player,xx,yy);
    
    //click listeners + movimentação
    btnLeft.addEventListener("click", function(){
        movePlayer(player,xx,yy,180);
    });
    btnUp.addEventListener("click", function(){
        movePlayer(player,xx,yy,90);
    });
    btnRight.addEventListener("click", function(){
        movePlayer(player,xx,yy,0);
    });
    btnDown.addEventListener("click", function(){
        movePlayer(player,xx,yy,270);
    });
    //botão de reset do localStorage, limpa as variáveis usadas 
    btnReset.addEventListener("click", function(){
        xx=0;
        yy=0;
        myStorage.clear(); 
        movePlayer(player,xx,yy,270);
    });
    
} else {
    // mensagem de erro
    console.log('Sem suporte a web storage')
}

//função de movimento do personagem
function movePlayer(_player,x,y,dir){
    //atualiza as coordenadas do jogador no DOM dentro dos limites do mapa
    switch(dir){
        case 0: if (xx<maxX) xx += spd; else xx=maxX; break;
        case 90: if (yy>minY) yy -= spd; else yy=minY; break;
        case 180: if (xx>minX) xx -= spd; else xx=minX; break;
        case 270: if (yy<maxY) yy += spd; else yy=maxY; break;
    }
    //css inline pra printar a posição do personagem na tela, de maneira responsiva
    player.setAttribute('style', 'left:calc(50% + '+x+'px) !important;top:calc(50% + '+y+'px) !important;');

    //após movimentar o player, memoriza suas coordenadas x e y pela função savePos
    savePos(myStorage,x,y);
}

//função que seta as variáveis do LocalStorage com as coordenadas do player
function savePos(storage,x,y){ 
    storage.setItem('playerX',x);
    storage.setItem('playerY',y);
}
    

//EXTRA: setas direcionais: https://stackoverflow.com/questions/5597060/detecting-arrow-key-presses-in-javascript
document.onkeydown = checkKey;

function checkKey(e) {
    e = e || window.event;
    if (e.keyCode == '38') {
        // up arrow
        movePlayer(player,xx,yy,90);
    }
    else if (e.keyCode == '40') {
        // down arrow
        movePlayer(player,xx,yy,270);
    }
    else if (e.keyCode == '37') {
       // left arrow
        movePlayer(player,xx,yy,180);
    }
    else if (e.keyCode == '39') {
       // right arrow
        movePlayer(player,xx,yy,0);
    }

}
    
    
/*
grass: https://opengameart.org/content/grass-texture-pack
hero: https://www.spriters-resource.com/pc_computer/rpgmaker95/sheet/100511/
mais sobre local storage: https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
*/